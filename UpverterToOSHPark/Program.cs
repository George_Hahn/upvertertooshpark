﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Ionic.Zip;

namespace UpverterToOSHPark
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: UpverterToOSHPark [upverter_zip_file.zip]");
                return;
            }

            string zip = args[0];

            string zipName = Path.GetFileNameWithoutExtension(zip);
            string zipDir = Path.GetDirectoryName(zip);
            string newFile = Path.Combine(zipDir, string.Format("osh_{0}.zip", zipName));

            if(File.Exists(newFile))
                File.Delete(newFile);

            ZipFile newZip = new ZipFile(newFile);

            using (var zipfile = ZipFile.Read(zip))
            {
                var filepairs = new TupleList<string, string>
                    {
                        {"layers.cfg", "layers.cfg"},
                        {"hole.ger", "Drills.xln"},
                        {"mechanical_details.ger", "Board Outline.ger"},

                        {"top_copper.ger", "Top Layer.ger"},
                        {"top_silkscreen.ger", "Top Silk Screen.ger"},
                        {"top_solder_mask.ger", "Top Solder Mask.ger"},

                        {"bottom_copper.ger", "Bottom Layer.ger"},
                        {"bottom_silkscreen.ger", "Bottom Silk Screen.ger"},
                        {"bottom_solder_mask.ger", "Bottom Solder Mask.ger"}
                    };

                foreach (var zipentry in zipfile)
                {
                    var files = from pair in filepairs
                                where zipentry.FileName.Contains(pair.Item1)
                                select pair.Item2;
                    if (files.Count() != 1)
                        continue;
                    string file = files.First();

                    var ms = new MemoryStream((int)zipentry.UncompressedSize);
                    zipentry.Extract(ms);

                    byte[] bytes = new byte[(int)ms.Length];
                    ms.Seek(0, SeekOrigin.Begin); // Rewind!
                    ms.Read(bytes, 0, (int)ms.Length);

                    string contents = Encoding.UTF8.GetString(bytes);
                    string[] lines = contents.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    if ((lines.Length == 5) && (lines[4] == "M02*")) // probably an empty file (*hopefully* :D)
                        continue;

                    newZip.AddEntry(file, bytes);
                    newZip.Save();
                }
            }
            Console.WriteLine("Done");
        }
    }
}
