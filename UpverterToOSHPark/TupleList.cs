﻿using System.Collections.Generic;

namespace UpverterToOSHPark
{
    public class TupleList<T1, T2> : List<LightTuple<T1, T2>>
    {
        public void Add(T1 item, T2 item2)
        {
            Add(new LightTuple<T1, T2>(item, item2));
        }
    }
}