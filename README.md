[UpverterToOSHPark](https://bitbucket.org/George_Hahn/upvertertooshpark/overview) ([Download](https://bitbucket.org/George_Hahn/upvertertooshpark/downloads/UpverterToOSHPark.msi))
=================

Converts Upverter gerberzips to OSHPark gerberzips

Usage: `UpverterToOSHPark [gerberzip].zip`
Output: `osh_[gerberzip].zip`

To be really clever, put a shortcut to this inside your `%APPDATA%\Microsoft\Windows\SendTo` folder
